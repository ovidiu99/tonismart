package application;

/*
CS-255 Getting started code for the assignment
I do not give you permission to post this code online
Do not post your solution online
Do not copy code
Do not use JavaFX functions or other libraries to do the main parts of the assignment:
	Gamma Correction
	Contrast Stretching
	Histogram calculation and equalisation
	Cross correlation
All of those functions must be written by yourself
You may use libraries to achieve a better GUI
*/
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.layout.FlowPane;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.PixelReader;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import java.lang.Math;

public class Photoshop extends Application {

 @Override
 public void start(Stage stage) throws FileNotFoundException {
  stage.setTitle("Photoshop");

  // Read the image
  Image image = new Image(new FileInputStream("raytrace.jpg"));

  // Create the graphical view of the image
  ImageView imageView = new ImageView(image);

  // Create the simple GUI
  Button invert_button = new Button("Invert");
  Button gamma_button = new Button("Gamma Correct");
  Button contrast_button = new Button("Contrast Stretching");
  Button histogram_button = new Button("Histograms");
  Button cc_button = new Button("Cross Correlation");
  Button myButton = new Button("Apply");

  Slider gammaSlider = new Slider(0.01, 6, 1);

  final Label gammaCorrection = new Label("Gamma correction: ");
  Label gammaValue = new Label(Double.toString(gammaSlider.getValue()));
  gammaValue.setTextFill(Color.BLACK);

  gammaSlider.valueProperty().addListener(new ChangeListener < Number > () {
    public void changed(ObservableValue < ? extends Number > ov, Number old_val, Number new_val) {

     gammaValue.setText(String.format("%.2f", new_val));
    }
   }

  );


  // Add all the event handlers (this is a minimal GUI - you may try to do better)
  invert_button.setOnAction(new EventHandler < ActionEvent > () {
   @Override
   public void handle(ActionEvent event) {
    System.out.println("Invert");
    // At this point, "image" will be the original image
    // imageView is the graphical representation of an image
    // imageView.getImage() is the currently displayed image

    // Let's invert the currently displayed image by calling the invert function
    // later in the code
    Image inverted_image = ImageInverter(imageView.getImage());
    // Update the GUI so the new image is displayed
    imageView.setImage(inverted_image);
   }
  });

  gamma_button.setOnAction(new EventHandler < ActionEvent > () {
   @Override
   public void handle(ActionEvent event) {
    System.out.println("Gamma Correction");

    Image gCorrected_image = GammaCorrection(imageView.getImage());
    // Update the GUI so the new image is displayed
    imageView.setImage(gCorrected_image);
   }
  });


  myButton.setOnAction(new EventHandler < ActionEvent > () {
   @Override
   public void handle(ActionEvent event) {
    System.out.println("Gamma Correct from Slider");
    Image gCorrected_imageS = GammaCorrectionSlider(imageView.getImage(), gammaSlider);
    imageView.setImage(gCorrected_imageS);
   }
  });

  contrast_button.setOnAction(new EventHandler < ActionEvent > () {
   @Override
   public void handle(ActionEvent event) {
    System.out.println("Contrast Stretching");
    
    Image conStretched_image = ContrastStretching(imageView.getImage());
    imageView.setImage(conStretched_image);
    
   }
  });

  histogram_button.setOnAction(new EventHandler < ActionEvent > () {
   @Override
   public void handle(ActionEvent event) {
    System.out.println("Histogram");
   }
  });

  cc_button.setOnAction(new EventHandler < ActionEvent > () {
   @Override
   public void handle(ActionEvent event) {
    System.out.println("Cross Correlation");
   }
  });

  // Using a flow pane
  FlowPane root = new FlowPane();
  // Gaps between buttons
  root.setVgap(10);
  root.setHgap(5);

  // Add all the buttons and the image for the GUI
  root.getChildren().addAll(invert_button, gamma_button, gammaCorrection, gammaSlider, gammaValue, myButton,
   contrast_button, histogram_button, cc_button, imageView);

  // Display to user
  Scene scene = new Scene(root, 1024, 768);
  stage.setScene(scene);
  stage.show();
 }

 // Example function of invert
 public Image ImageInverter(Image image) {
  // Find the width and height of the image to be process
  int width = (int) image.getWidth();
  int height = (int) image.getHeight();
  // Create a new image of that width and height
  WritableImage inverted_image = new WritableImage(width, height);
  // Get an interface to write to that image memory
  PixelWriter inverted_image_writer = inverted_image.getPixelWriter();
  // Get an interface to read from the original image passed as the parameter to
  // the function
  PixelReader image_reader = image.getPixelReader();

  // Iterate over all pixels
  for (int y = 0; y < height; y++) {
   for (int x = 0; x < width; x++) {
    // For each pixel, get the colour
    Color color = image_reader.getColor(x, y);
    // Do something (in this case invert) - the getColor function returns colours as
    // 0..1 doubles (we could multiply by 255 if we want 0-255 colours)
    color = Color.color(1.0 - color.getRed(), 1.0 - color.getGreen(), 1.0 - color.getBlue());
    // Note: for gamma correction you may not need the divide by 255 since getColor
    // already returns 0-1, nor may you need multiply by 255 since the Color.color
    // function consumes 0-1 doubles.

    // Apply the new colour
    inverted_image_writer.setColor(x, y, color);
   }
  }
  return inverted_image;
 }

 // function for gamma correction
 public Image GammaCorrection(Image image) {
  // Find the width and height of the image to be processed
  int width = (int) image.getWidth();
  int height = (int) image.getHeight();
  // Create a new image of that width and height
  WritableImage gCorrected_image = new WritableImage(width, height);
  // Get an interface to write to that image memory
  PixelWriter gCorrected_image_writer = gCorrected_image.getPixelWriter();
  // Get an interface to read from the original image passed as the parameter to
  // the function
  PixelReader image_reader = image.getPixelReader();

  // Iterate over all pixels
  for (int y = 0; y < height; y++) {
   for (int x = 0; x < width; x++) {
    // For each pixel, get the colour
    Color color = image_reader.getColor(x, y);
    // Do something(in this case gamma correction) - the getColor function returns
    // colours as 0..1 doubles

    color = Color.color(Math.pow(color.getRed(), 1.0 / 2.8), Math.pow(color.getGreen(), 1.0 / 2.8),
     Math.pow(color.getBlue(), 1.0 / 2.8));
    // Apply gamma correction
    gCorrected_image_writer.setColor(x, y, color);
   }
  }
  return gCorrected_image;
 }
 // function for gamma correction slider
 public Image GammaCorrectionSlider(Image image, Slider gammaSlider) {
  double myGammaValue = gammaSlider.getValue();
  // Find the width and height of the image to be processed
  int width = (int) image.getWidth();
  int height = (int) image.getHeight();
  // Create a new image of that width and height
  WritableImage gCorrected_imageS = new WritableImage(width, height);
  // Get an interface to write to that image memory
  PixelWriter gCorrected_image_writer = gCorrected_imageS.getPixelWriter();
  // Get an interface to read from the original image passed as the parameter to
  // the function
  PixelReader image_reader = image.getPixelReader();

  // Iterate over all pixels
  for (int y = 0; y < height; y++) {
	  for (int x = 0; x < width; x++) {
    // For each pixel, get the colour
    Color color = image_reader.getColor(x, y);
    // Do something(in this case gamma correction) - the getColor function returns
    // colours as 0..1 doubles

    color = Color.color(Math.pow(color.getRed(), 1.0 / myGammaValue), Math.pow(color.getGreen(), 1.0 / myGammaValue),
     Math.pow(color.getBlue(), 1.0 / myGammaValue));
    // Apply gamma correction
    gCorrected_image_writer.setColor(x, y, color);
   }
  }
  return gCorrected_imageS;
 }

   // function for Contrast Stretching
   public Image ContrastStretching(Image image) {
    // find the width and the height of the image to be processed
   int width = (int) image.getWidth();
    int height = (int) image.getHeight();
    
   // Create a new image of that width and height
   WritableImage conStretched_image = new WritableImage(width, height);
   // Get an interface to write to that image memory.
   PixelWriter conStretched_image_write = conStretched_image.getPixelWriter();
   // Get an interface to read from the original image passed as the parameter to the function
    PixelReader image_reader = image.getPixelReader();
    
    int r1 = 50;
    int s1 = 20;
    
    int r2 = 200;
    int s2 = 225;
    
    // Iterate over all pixels
    for (int y = 0; y < height; y++) {
    		for (int x = 0; x < width; x++) {
    // for each pixel, get the color
   Color color = image_reader.getColor(x, y);
   
   //CASE 1
    if(color.getRed() * 255 < r1 && color.getGreen() * 255 < r1 && color.getBlue() * 255 < r1) {
    	
	   color = Color.color(s1 / r1 * color.getRed(), s1 / r1 * color.getGreen(), s1 / r1 * color.getBlue());
   }
    
   //CASE 2
    if(r1 <= color.getRed() * 255 && color.getRed() * 255 <= r2 && 
    	r1 <= color.getGreen() * 255 && color.getGreen() * 255 <= r2 && 
        r1 <= color.getBlue() * 255 && color.getBlue() * 255 <= r2 ) {
    		
    		color = Color.color( ((s2-s1) / (r2-r1) * (color.getRed() * 255  - r1) + s1)/255,
    							 ((s2-s1) / (r2-r1) * (color.getGreen() * 255  - r1) + s1)/255,
    							 ((s2-s1) / (r2-r1) * (color.getBlue() * 255 - r1) + s1)/255); 
    }
   
    //CASE 3
    if(color.getRed() * 255 >= r2 && color.getGreen() * 255 >= r2 && color.getBlue() * 255 >= r2) {
    	
    	color = Color.color(    ((255-s2) / (255-r2) * (color.getRed() * 255  - r2) + s2)/255
    							,((255-s2) / (255-r2) * (color.getGreen() * 255  - r2) + s2)/255
    							,((255-s2) / (255-r2) * (color.getBlue() * 255  - r2) + s2)/255);
    }
    
  
   
    conStretched_image_write.setColor(x, y, color);
    
    		}
    			}
       return conStretched_image;
   }

  
  
   
   
  

 public static void main(String[] args) {
  launch();
 }

}

